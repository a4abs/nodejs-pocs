const fs = require("fs");
const pdf = require("pdf-parse");
const pdftk = require("node-pdftk");
const PDFParser = require("pdf2json");
const pdfParser = new PDFParser();

const fileType1Name = "PO to Import type1-TEKMY.pdf";
const fileType2Name = "PO to Import type2-OFFICINA_c.pdf";
const fileType3Name = "PO to Import type3- MDD.pdf";

// We can change file name to parse PDF files
const pdfDataBuffer = fs.readFileSync(
  __dirname + `/input-files/${fileType1Name}`
);

pdf(pdfDataBuffer).then(function (data) {
  // number of pages
  console.log(data.numpages);

  // number of rendered pages
  console.log(data.numrender);

  // PDF info
  console.log(data.info);

  // PDF metadata
  console.log(data.metadata);

  // PDF.js version
  // check https://mozilla.github.io/pdf.js/getting_started/
  console.log(data.version);

  // PDF text
  console.log(data.text);
});

// pdftk
//   .input(__dirname + `/input-files/${fileType1Name}`)
//   .dumpDataFieldsUtf8()
//   .output();

pdfParser.on("pdfParser_dataError", (errData) => console.error("err"));
pdfParser.on("pdfParser_dataReady", (pdfData) => {
  fs.writeFile("F1040EZ.json", JSON.stringify(pdfData), (response) => {
    console.log(response);
  });
});

pdfParser.loadPDF(__dirname + `/input-files/${fileType1Name}`);
